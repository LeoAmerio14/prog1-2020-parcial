﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial
{
    class EnviosFragil : Envio
    {
        public int FechaEstimada { get; set; }
        public double Costo { get; set; }
        public int Tipo { get; set; } // 1 inflamable 2 toxico 3 explosivo

        public override Envio GenerarCodigo()
        {
            var random = new Random(6);
            var codigo = random;
        }

        public override List<Envio> ObtenerCostoYDemora()
        {
            List<Envio> Envios = new List<Envio>();
            int costo;
            int fechaEstimada = FechaPedido + 5;
            EnviosFragil envio = new EnviosFragil();
            if (Tipo == 1 || Tipo == 2)
            {
                if (CodigoPostalDestino != CodigoPostalOrigen)
                {
                    costo = (240 * 7) / 100 + 240;
                }
                else
                {
                    costo = 240;
                }
            }
            else
            {
                if (CodigoPostalDestino != CodigoPostalOrigen)
                {
                    costo = (275 * 7) / 100 + 275;
                }
                else
                {
                    costo = 275;
                }
            }
            envio.Costo = costo;
            envio.FechaEstimada = fechaEstimada;
            Envios.Add(envio);
            return Envios;
        }
    }
}
