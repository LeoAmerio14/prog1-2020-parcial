﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial
{
    abstract class Envio
    {
        public int CodigoPostalOrigen { get; set; }
        public int CodigoPostalDestino { get; set; }
        public int CodigoSeguimiento { get; set; }
        public double VolumenPaquete { get; set; }
        public double Peso { get; set; }
        public int FechaPedido { get; set; }
        public int FechaEntrega { get; set; }
        public string NombreCliente { get; set; }

        //CORRECCIÓN: ESTA BIEN QUE SEA UN SOLO MÉTODO, PERO NO DEBÍA DEVOLVER NADA, DEBÍA GUARDAR SOBRE LAS MISMAS PROPIEDADES.
        public abstract List<Envio> ObtenerCostoYDemora();
        public abstract Envio GenerarCodigo(); 
    }
}
