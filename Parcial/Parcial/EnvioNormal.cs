﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Parcial
{
    class EnvioNormal : Envio
    {
        //CORRECCIÓN: LA FECHA Y EL COSTO, SI SE REPITEN EN TODAS LAS SUBCLASES ENTONCES SON PROPIEDADES DE LA CLASE "ENVIO"
        public int FechaEstimada { get; set; }
        public double Costo { get; set; }

        //CORRECCIÓN: ESTA LISTA ESTA MAL, SOLO LA CLASE PRINCIPAL DEBERÍA TENER LISTAS DONDE ALMACENAR PERO ADEMAS NO SE NECESITA RETORNAR UN LISTADO DE ENVIOS
        List<Envio> Envios = new List<Envio>();
        public override List<Envio> ObtenerCostoYDemora()
        {
            //CORRECCIÓN: INCORRECTO, SE YA EN LA CLASE EN SI MISMA UN OBJETO TIPO ENVIONORMAL, TENES QUE TRABAJAR CON SUS PROPIEDADES, NO CREAR OTRO OBJETO DEL MISMO TIPO.
            EnvioNormal envio = new EnvioNormal();
            //return (double)VolumenPaquete * 10.25 + (double)Peso * 5.75
            int fechaEstimada = FechaPedido + 3;
            double costo = (VolumenPaquete * 10.25 + Peso * 5.75);
            envio.FechaEstimada = fechaEstimada;
            envio.Costo = costo;
            Envios.Add(envio);
            return Envios;
        }

        //INCOMPLETO
        public override Envio GenerarCodigo()
        {
            var random = new Random(6);
            var codigo = random;
        }
    }
}
