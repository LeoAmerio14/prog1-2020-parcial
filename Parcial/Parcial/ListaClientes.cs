﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial
{
    class ListaClientes
    {
        public string Nombre { get; set; }
        public int FechaIngreso { get; set; }
        public bool Entregado { get; set; }
    }
}
