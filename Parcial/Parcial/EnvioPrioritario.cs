﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial
{
    class EnvioPrioritario : Envio
    {
        public int FechaEstimada { get; set; }
        public double Costo { get; set; }

        public override Envio GenerarCodigo()
        {
            var random = new Random(6);
            var codigo = random;
        }

        public override List<Envio> ObtenerCostoYDemora()
        {
            //CORRECCIÓN: OBTENERCOSTOYDEMORA TIENE QUE TRABAJAR CON LAS PROPRIEDADES DE LA CLASE, DEBE RESLVER ESTO SOLO PARA UN OBJETO, NO DEVOLVER UN LISTADO.
            List<Envio> Envios = new List<Envio>();
            double costo = 0;
            int fechaEstimada = 0;
            int cont = 0;

            //CORRECCIÓN: YA LO DIJE EN OTRA CLASE, ESTO NO ESTA BIEN, TENES QUE TRABAJAR CON LAS PROPIEDADES DEL OBJETO.
            EnvioPrioritario envio = new EnvioPrioritario();
            if (CodigoPostalDestino == CodigoPostalOrigen)
            {
                fechaEstimada = FechaPedido + 1;
                for (int i = 0; i < Peso; i++)
                {
                    if (Peso >= 5)
                    {
                        //CORRECCIÓN: ESTO ESTÁ MAL PORQUE MODIFICAS LOS VALORES REALES DEL OBJETO.
                        Peso--;
                        cont++;
                    }
                }
                costo = 120 + 12.5 * cont;
            }
            else
            {
                fechaEstimada = FechaPedido + 2;
                for (int i = 0; i < Peso; i++)
                {
                    if (Peso >= 5)
                    {
                        Peso--;
                        cont++;
                    }
                }
                costo = 180 + 12.5 * cont;
            }
            
            //CORRECCIÒN: HUBIERA ESTADO BIEN SI LO HACIAS SOBRE LAS PRIPIEDADES DEL MISMO OBJETO, ASÍ ESTA MAL.
            envio.Costo = costo;
            envio.FechaEstimada = fechaEstimada;
            Envios.Add(envio);
            return Envios;

        }
    }
}
